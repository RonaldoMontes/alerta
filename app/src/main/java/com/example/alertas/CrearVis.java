package com.example.alertas;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.alertas.Api.Api;
import com.example.alertas.Api.Servicios.ServicioPeticion;
import com.example.alertas.ViewModels.Peticion_Crear;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CrearVis extends AppCompatActivity {
    private Button Back, Crear;
    private EditText alerta;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crear_vis);
        alerta = (EditText) findViewById(R.id.editText6);
        Back = (Button) findViewById(R.id.button13);
        Crear = (Button) findViewById(R.id.button12);

        Back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), Menu.class);
                startActivity(intent);

            }
        });

        Crear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences preferencias = getSharedPreferences("credenciales", Context.MODE_PRIVATE);
                ServicioPeticion s = Api.getApi(CrearVis.this).create(ServicioPeticion.class);
                String id = preferencias.getString("ID", "");
                int ids= Integer.parseInt(id.toString());
                int idsA = Integer.parseInt(alerta.getText().toString());
                Call<Peticion_Crear> Registros = s.getVisA(ids,idsA);
                Registros.enqueue(new Callback<Peticion_Crear>() {
                    @Override
                    public void onResponse(Call<Peticion_Crear> call, Response<Peticion_Crear> response) {
                        Peticion_Crear mostrar = response.body();
                        String Cadena = "";
                        if (mostrar.estado.equals("true")){

                            Toast.makeText(CrearVis.this, "Vizualizacion Creada con Extito", Toast.LENGTH_SHORT).show();

                        }else{
                            Toast.makeText(CrearVis.this, "Datos Incorrectos", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<Peticion_Crear> call, Throwable t) {
                        Toast.makeText(CrearVis.this, "Error", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
    }
}

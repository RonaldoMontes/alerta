package com.example.alertas;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.alertas.Api.Api;
import com.example.alertas.Api.Servicios.ServicioPeticion;
import com.example.alertas.ViewModels.AlertasUs;
import com.example.alertas.ViewModels.Peticion_Alertas;
import com.example.alertas.ViewModels.Peticion_Crear;
import com.example.alertas.ViewModels.Peticion_VisU;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.firebase.messaging.FirebaseMessaging;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Menu extends AppCompatActivity {
    private Button Crear, AlertasU, Alertas, VisualizacionesU, Visualizaciones, Limpiar, Salir, CrearNot;
    private EditText caja;
    public String APITOKEN;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        Crear = (Button) findViewById(R.id.button5);
        AlertasU = (Button) findViewById(R.id.button6);
        Alertas = (Button) findViewById(R.id.button7);
        VisualizacionesU = (Button) findViewById(R.id.button8);
        Visualizaciones = (Button) findViewById(R.id.button9);
        Limpiar = (Button) findViewById(R.id.button10);
        Salir = (Button) findViewById(R.id.button11);
        caja = (EditText) findViewById(R.id.editText5);
        CrearNot = (Button) findViewById(R.id.button14);
        
        FirebaseMessaging.getInstance().subscribeToTopic("atodos").addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                Toast.makeText(Menu.this, "Se agrego a la lista", Toast.LENGTH_SHORT).show();
            }
        });
        CrearNot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                llamarGeneral();
                Toast.makeText(Menu.this, "Hola", Toast.LENGTH_SHORT).show();
            }
        });

        Limpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                caja.setText("");
            }
        });
        Salir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences preferencias = getSharedPreferences("credenciales",Context.MODE_PRIVATE);
                String tokenn = APITOKEN;
                SharedPreferences.Editor editor = preferencias.edit();
                editor.putString("TOKEN","");
                editor.commit();
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);
            }
        });

        Crear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences preferencias = getSharedPreferences("credenciales", Context.MODE_PRIVATE);
                ServicioPeticion s = Api.getApi(Menu.this).create(ServicioPeticion.class);
                String id = preferencias.getString("ID", "");
                int ids= Integer.parseInt(id.toString());
                Call<Peticion_Crear> Registros = s.getAlert(ids);
                Registros.enqueue(new Callback<Peticion_Crear>() {
                    @Override
                    public void onResponse(Call<Peticion_Crear> call, Response<Peticion_Crear> response) {
                        Peticion_Crear mostrar = response.body();
                        String Cadena = "";
                        if (mostrar.estado.equals("true")){

                            Toast.makeText(Menu.this, "Se creo Correctamente", Toast.LENGTH_SHORT).show();

                        }else{
                            Toast.makeText(Menu.this, "Datos Incorrectos", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<Peticion_Crear> call, Throwable t) {
                        Toast.makeText(Menu.this, "Error", Toast.LENGTH_SHORT).show();
                    }
                });



            }
        });

        AlertasU.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences preferencias = getSharedPreferences("credenciales", Context.MODE_PRIVATE);
                ServicioPeticion s = Api.getApi(Menu.this).create(ServicioPeticion.class);
                String id = preferencias.getString("ID", "");
                int ids= Integer.parseInt(id.toString());
                Call<AlertasUs> Registros = s.getAlertU(ids);
                Registros.enqueue(new Callback<AlertasUs>() {
                    @Override
                    public void onResponse(Call<AlertasUs> call, Response<AlertasUs> response) {
                        AlertasUs mostrar = response.body();
                        String Cadena = "";
                        if (mostrar.estado.equals("true")){

                            for (Datos elemento: mostrar.alertas
                            ) {
                                Cadena = Cadena + elemento.id + " | " + elemento.usuarioId + " | " + elemento.created_at + " | " + elemento.updated_at + " \n ";
                                caja.setText(Cadena);
                            }

                        }else{
                            Toast.makeText(Menu.this, "Datos Incorrectos", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<AlertasUs> call, Throwable t) {
                        Toast.makeText(Menu.this, "Error", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
        Alertas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ServicioPeticion s = Api.getApi(Menu.this).create(ServicioPeticion.class);

                Call<Peticion_Alertas> Registros = s.getAlertas();
                Registros.enqueue(new Callback<Peticion_Alertas>() {
                    @Override
                    public void onResponse(Call<Peticion_Alertas> call, Response<Peticion_Alertas> response) {
                        Peticion_Alertas mostrar = response.body();
                        String Cadena = "";
                        if (!mostrar.alertas.isEmpty()){

                            for (Datos elemento: mostrar.alertas
                            ) {
                                Cadena = Cadena + elemento.id + " | " + elemento.usuarioId + " | " + elemento.created_at + " | " + elemento.updated_at + " \n ";
                                caja.setText(Cadena);
                            }

                        }else{
                            Toast.makeText(Menu.this, "Datos Incorrectos", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<Peticion_Alertas> call, Throwable t) {
                        Toast.makeText(Menu.this, "Error", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });

        VisualizacionesU.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences preferencias = getSharedPreferences("credenciales", Context.MODE_PRIVATE);
                ServicioPeticion s = Api.getApi(Menu.this).create(ServicioPeticion.class);
                String id = preferencias.getString("ID", "");
                int ids= Integer.parseInt(id.toString());
                Call<Peticion_VisU> Registros = s.getVisU(ids);
                Registros.enqueue(new Callback<Peticion_VisU>() {
                    @Override
                    public void onResponse(Call<Peticion_VisU> call, Response<Peticion_VisU> response) {
                        Peticion_VisU mostrar = response.body();
                        String Cadena = "";
                        if (mostrar.estado.equals("true")){

                            for (Datos elemento: mostrar.visualizaciones
                            ) {
                                Cadena = Cadena + elemento.id + " | " + elemento.usuarioId + " | "  + elemento.alertaId + " | "+ elemento.created_at + " | " + elemento.updated_at + " \n ";
                                caja.setText(Cadena);
                            }

                        }else{
                            Toast.makeText(Menu.this, "Datos Incorrectos", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<Peticion_VisU> call, Throwable t) {
                        Toast.makeText(Menu.this, "Error", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });

        Visualizaciones.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ServicioPeticion s = Api.getApi(Menu.this).create(ServicioPeticion.class);

                Call<Peticion_VisU> Registros = s.getVis();
                Registros.enqueue(new Callback<Peticion_VisU>() {
                    @Override
                    public void onResponse(Call<Peticion_VisU> call, Response<Peticion_VisU> response) {
                        Peticion_VisU mostrar = response.body();
                        String Cadena = "";
                        if (!mostrar.visualizaciones.isEmpty()){

                            for (Datos elemento: mostrar.visualizaciones
                            ) {
                                Cadena = Cadena + elemento.id + " | " + elemento.usuarioId + " | "  + elemento.alertaId + " | "+ elemento.created_at + " | " + elemento.updated_at + " \n ";
                                caja.setText(Cadena);
                            }

                        }else{
                            Toast.makeText(Menu.this, "Datos Incorrectos", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<Peticion_VisU> call, Throwable t) {
                        Toast.makeText(Menu.this, "Error", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });

    }
    private void llamarGeneral(){
        RequestQueue myrequest = Volley.newRequestQueue(getApplicationContext());
        JSONObject json = new JSONObject();
        try {
            json.put("to","/topics/"+"atodos");
            JSONObject notificaciones = new JSONObject();
            notificaciones.put("titulo", "Hola");
            notificaciones.put("detalle","Vato");
            json.put("data",notificaciones);
            String URL = "https://fcm.googleapis.com/fcm/send";
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST,URL,json,null, null){
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> header = new HashMap<>();
                    header.put("content-type","application/json");
                    header.put("authorization","key=AAAA0BSlSlY:APA91bEGlmxjLB3ltON5gCqJSssoCIJ28sSywFLG1xAEcH-KP4IEDIIxYDnV0dfV7jvkVM5L-I7GGCEObq2k0rWjYcUzIEHyD4NabzTORcESubh_zdxqa26g52_KeotZ53OQIfi9UTOi");
                    return header;
                }
            };
            myrequest.add(request);
        }catch (JSONException e){
            e.printStackTrace();
        }
    }
}

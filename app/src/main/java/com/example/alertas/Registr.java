package com.example.alertas;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.alertas.Api.Api;
import com.example.alertas.Api.Servicios.ServicioPeticion;
import com.example.alertas.ViewModels.Peticion_Registro;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Registr extends AppCompatActivity {
    private Button Reg;
    private EditText Correo, Password;
    private Button btn4;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registr);
        Reg=(Button)findViewById(R.id.button3);
        Correo=(EditText)findViewById(R.id.editText3);
        Password=(EditText)findViewById(R.id.editText4);
        btn4=(Button)findViewById(R.id.button4);
        btn4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent (getApplicationContext(), MainActivity.class);
                startActivity(intent);
            }
        });
        Reg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ServicioPeticion service = Api.getApi(Registr.this).create(ServicioPeticion.class);
                Call<Peticion_Registro> registrarCall =  service.registrarUsuario(Correo.getText().toString(),Password.getText().toString());
                registrarCall.enqueue(new Callback<Peticion_Registro>() {
                    @Override
                    public void onResponse(Call<Peticion_Registro> call, Response<Peticion_Registro> response) {
                        Peticion_Registro peticion = response.body();
                        if(response.body() == null){
                            Toast.makeText(Registr.this, "Ocurrio un Error, intentalo más tarde", Toast.LENGTH_LONG).show();
                            return;
                        }
                        if(peticion.estado == "true"){
                            startActivity(new Intent(Registr.this,MainActivity.class));
                            Toast.makeText(Registr.this, "Datos Registrados", Toast.LENGTH_LONG).show();
                        }else{
                            Toast.makeText(Registr.this, peticion.detalle, Toast.LENGTH_LONG).show();
                        }
                    }
                    @Override
                    public void onFailure(Call<Peticion_Registro> call, Throwable t) {
                        Toast.makeText(Registr.this, "Error", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
    }
}

package com.example.alertas.ViewModels;

import com.example.alertas.Datos;

import java.util.List;

public class Peticion_Alertas {
    public List<Datos> alertas;

    public List<Datos> getAlertas() {
        return alertas;
    }

    public void setAlertas(List<Datos> alertas) {
        this.alertas = alertas;
    }
}

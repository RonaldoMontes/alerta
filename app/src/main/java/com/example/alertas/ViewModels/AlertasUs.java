package com.example.alertas.ViewModels;

import com.example.alertas.Datos;

import java.util.List;

public class AlertasUs {
    public String estado;
    public List<Datos> alertas;

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public List<Datos> getAlertas() {
        return alertas;
    }

    public void setAlertas(List<Datos> alertas) {
        this.alertas = alertas;
    }
}

package com.example.alertas.ViewModels;

import com.example.alertas.Datos;

import java.util.List;

public class Peticion_VisU {
    public String estado;
    public List<Datos> visualizaciones;

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public List<Datos> getVisualizaciones() {
        return visualizaciones;
    }

    public void setVisualizaciones(List<Datos> visualizaciones) {
        this.visualizaciones = visualizaciones;
    }
}
